# Project storybook

View: [storybook test](http://test.andresmorenostudio.com "storybook")

# Stack

React, styled-component, storybook

# Install dependencies

`yarn install`

# Compile storybook

`yarn build-storybook`


