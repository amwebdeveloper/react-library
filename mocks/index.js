export const options = [
    {
        name: 'Firts link',
        link: ''
    },
    {
        name: 'Second link',
        link: ''
    },
    {
        name: 'Third link',
        link: ''
    },
    {
        name: 'Four link',
        link: ''
    }
];

export const kpiOptions = [
    {
        percentage: 70,
        dataName: "Fake data name"
    },
    {
        percentage: 62,
        dataName: "Fake data name"
    },
    {
        percentage: 32,
        dataName: "Fake data name"
    }
];

export const buttonOptions = [
    {
        description: 'Calendar'
    },
    {
        description: 'Stats'
    },
    {
        description: 'Diffusions'
    },
    {
        description: 'Calendar'
    },
    {
        description: 'Stats'
    },
    {
        description: 'Diffusions'
    }
];

export const tabs = [
    {
        name: 'First tab',
        shortName: 'first',
        content: "Content first tab"
    },
    {
        name: 'Second tab',
        shortName: 'second',
        content: "Content second tab"
    },
    {
        name: 'Third tab',
        shortName: 'third',
        content: "Content third tab"
    },
    {
        name: 'Fourth tab',
        shortName: 'fourth',
        content: "Content fourth tab"
    }
]