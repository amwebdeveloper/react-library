import styled, { css } from 'styled-components';
import { rem } from '../../utils/functions/';

const Wrapper = styled.div`
    display: flex;
    align-items: center;

    &:before {
        transition: border 0.2s ease;
        content: "";
        display: block;
        height: ${rem('24px')};
        box-sizing: border-box;
        line-height: 0;
        border-left: 15px solid transparent;
        border-right: 15px solid transparent;
        border-top: 25px solid ${props => props.theme.colors.red};

        margin-right: ${rem('10px')};
    }

    ${props => props.good >= 50 && css`
        &:before {
            border-top: 0;
            border-bottom: 25px solid ${props => props.theme.colors.green};
        }
    `}
`;

const Percentage = styled.span`
    font-size: ${rem('30px')};
    display: block;
    font-weight: bold;
    @media only screen and (min-width: ${rem('768px')}) {
        font-size: ${rem('56px')};
    }
`;

const DataName = styled.span`
    display: block;
    color: grey;
`;

Wrapper.defaultProps = {
    theme: {
        colors: {
            red: 'red',
            green: 'lightgreen'
        }
    }
};

export { Wrapper, Percentage, DataName };
