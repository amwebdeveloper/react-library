import React, {Component} from 'react';
import { Wrapper, Percentage, DataName } from './styled'

class Kpi extends Component {
    constructor(props) {
        super(props);
        this.state = {percentage: 0};
    }

    componentDidMount() {
        const {percentage} = this.props;
        this.interval = setInterval(() => {
            this.state.percentage < percentage && this.updatePercentage();
        }, 0);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    updatePercentage() {
        this.setState({percentage: this.state.percentage + 1});
    }

    render() {
        const {dataName} = this.props;
        return(
            <Wrapper good={parseInt(this.state.percentage)}>
                <div>
                    <Percentage>{parseInt(this.state.percentage)}%</Percentage>
                    <DataName>{dataName}</DataName>
                </div>
            </Wrapper>
        );
    }
}
export default Kpi;
