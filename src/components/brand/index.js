import React from 'react';
import { StyleH1, StyleLink } from './styled';

const Brand = ({ text }) => (
    <StyleH1><StyleLink href="/">{text}</StyleLink></StyleH1>
);

export default Brand;
