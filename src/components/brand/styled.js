import styled from 'styled-components';
import { rem } from '../../utils/functions/';

const StyleH1 = styled.h1`
    font-size: ${rem('22px')};
`;

const StyleLink = styled.a`
    text-decoration: none;
    color: inherit;
    display: block;
`;

export { StyleH1, StyleLink };
