import styled, { css } from 'styled-components';
import { rem } from '../../utils/functions/';

const Wrapper = styled.div`
    will-change: max-height, opacity;
    position: relative;
    color: darkgray;
    max-height: ${rem('280px')};
    overflow: hidden;
    transition: all 0.15s ease;

    &:after {
        content: "";
        position: absolute;
        left: 0;
        bottom: 0;
        width: 100%;
        height: ${rem('100px')};
        background: linear-gradient(rgba(255,255,255,0.2), rgba(255,255,255,0.9));
        opacity: 1;
    }

    // state
    ${props => props.show && css`
        max-height: ${rem('1500px')};

        &:after {
            opacity: 0;
        }
    `}
`;

const ButtonDesc = styled.button`
    margin: auto;
    position: relative;
    display: flex;
    background: none;
    border: 0;
    font-family: inherit;
    font-size: inherit;
    cursor: pointer;
    padding: ${rem('8px')} ${rem('20px')};
`;

const ImgButton = styled.img.attrs(props => ({
    alt: "",
    width: "24",
    height: "24"
}))`
    ${props => props.show && css`
        transform: rotate(180deg);
    `}
`;

export { Wrapper, ButtonDesc, ImgButton };
