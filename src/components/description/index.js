import React, { Component } from 'react';
import { Wrapper, ButtonDesc, ImgButton } from './styled'
import { ArrowDown } from '../../../assets';

class Description extends Component {
    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
        this.state = { show: false };
        this.text = "View more";
    }

    onClick() {
        this.setState({ show: !this.state.show });
        this.text = !this.state.show ? "View less" : "View more";
    }

    render() {
        const { children } = this.props;
        return (<div>
            <Wrapper show={this.state.show}>
                {children}
            </Wrapper>
            <ButtonDesc onClick={this.onClick}>
                <ImgButton
                    src={ArrowDown}
                    show={this.state.show}
                />
                {this.text}
            </ButtonDesc>
        </div>);
    }
}
export default Description;
