import Tab from './tab';
import Tabs from './tabs';
import TabContent from './tabContent';
import TabBehaviour from './tabBehaviour';

export { Tabs, Tab, TabContent, TabBehaviour };