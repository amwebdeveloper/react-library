import React from 'react';
import { TabsStyle } from './styled';

const Tabs = ({ children }) => (
    <TabsStyle>
        {children}
    </TabsStyle>
);

export default Tabs;
