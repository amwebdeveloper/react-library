import React, {Component} from 'react';
import {Tab, TabContent, Tabs} from './';

class TabsBehaviour extends Component {
    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
        this.state = {active: 0};
        this.beforeActive = 0;
    }

    onClick(tab) {
        this.beforeActive = this.state.active;
        this.setState({active: tab.props.index});
    }

    render() {
        const { children } = this.props;
        return (
            <div>
                <Tabs>
                    {children.map((opt, idx) => {
                        return (
                            <Tab
                                key={idx}
                                elem={`tabcontent-` + idx}
                                id={`tab-`+idx}
                                onClick={this.onClick}
                                label={opt.props.label}
                                active={idx === this.state.active}
                                index={idx}
                                numTab={children.length}
                                beforeActive={this.beforeActive}
                            />
                        );
                    })}
                </Tabs>

                {children.map((child, idx) => {
                    return (
                        <TabContent
                            id={`tabcontent-` + idx}
                            key={idx}
                            active={idx === this.state.active}
                            elem={`tab-`+idx}
                        >
                            {child.props.children}
                        </TabContent>
                    );
                })}
            </div>
        );
    }
}

export default TabsBehaviour;
