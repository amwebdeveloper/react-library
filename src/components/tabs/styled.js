import styled, { css, keyframes } from 'styled-components';
import { rem } from '../../utils/functions/';

const move = (x) => keyframes`
    from {
        transform: translateX(${x}00%);
    }
    to {
        transform: translateX(0%);
    }
`;


const TabsStyle = styled.ul.attrs(props => ({
    role: 'tablist'
}))`
    margin: 0;
    padding: 0;
    display: flex;
    border-bottom: ${rem('2px')} solid;
    color: ${props => props.theme.colors.lightgrey};
    max-width: 100%;
    overflow-y: hidden;
    overflow-x: auto;

    @media only screen and (min-width: ${rem('1024px')}) {
        overflow: initial;
    }
`;

const TabStyle = styled.li.attrs(props => ({
    role: "tab",
    tabIndex: '0',
    "aria-expanded": props.active || false,
    "aria-selected": props.active || false,
    "aria-controls": props.elem || ""
}))`
    position: relative;
    top: ${rem('2px')};
    display: block;
    list-style: none;
    padding: ${rem('12px')} ${rem('10px')} ${rem('10px')};
    border-bottom: ${rem('2px')} solid;
    padding: ${rem('18px')} ${rem('25px')};
    min-width: calc(100% / ${props => props.numTab});
    max-width: calc(100% / ${props => props.numTab});
    text-align: center;
    cursor: pointer;

    &:after {
        content: "";
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        margin: auto;
        width: ${rem('2px')};
        border-radius: ${rem('5px')};
        height: 25%;
        background-color: ${props => props.theme.colors.lightgrey};
    }

    &:before {
        will-change: transform;
        position: absolute;
        content: "";
        width: 100%;
        left: 0;
        bottom: ${rem('-2px')};
        height: ${rem('2px')};
        z-index: 1;
    }

    &:last-child {
        &:after {
            content: none;
        }
    }

    ${props => props.active && css`
        color: ${props => props.theme.colors.font};
        border-color: transparent;
        cursor: default;

        &:before {
            background: ${props => props.theme.colors.bgradient};
            animation: ${move(props.beforeActive - props.index)} .25s linear;
        }
    `}

    @media only screen and (min-width: ${rem('1024px')}) {
        padding: ${rem('18px')} ${rem('46px')};
        min-width: 0;
    }
`;

const TabContentStyle = styled.div.attrs(props => ({
    role: "tabpannel",
    "aria-labelledby": props.elem,
    "aria-hidden": props.active
}))`
    padding: ${rem('46px')};
    min-height: 20rem;
    display: none;

    &[aria-hidden="false"] {
        display: block;
    }
`;

TabsStyle.defaultProps = {
    theme: {
        colors: {
            lightgrey: 'lightgrey'
        }
    }
};

TabStyle.defaultProps = {
    theme: {
        colors: {
            font: 'black',
            lightgrey: 'lightgrey',
            bgradient: "linear-gradient(90deg, #BBB 0%, #999 100%)"
        }
    }
};

export { TabsStyle, TabStyle, TabContentStyle };
