import React, { Component } from 'react';
import { TabStyle } from './styled';

class Tab extends Component {
    
    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }

    onClick() {
        const {onClick} = this.props;
        onClick(this);
    }

    render() {
        const {active, id, elem, label, numTab, index, beforeActive} = this.props
        return(
            <TabStyle
                active={active}
                elem={elem}
                id={id}
                onClick={this.onClick}
                index={index}
                numTab={numTab}
                beforeActive={beforeActive}
            >
                {label}
            </TabStyle>
        );
    }
}

export default Tab;
