import React from 'react';
import { TabContentStyle } from './styled'

const TabContent = ({children, active, elem, id}) =>
    <TabContentStyle
        active={!active}
        elem={elem}
        id={id}
    >
        {children}
    </TabContentStyle>
;

export default TabContent;
