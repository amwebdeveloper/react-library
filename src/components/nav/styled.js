import styled from 'styled-components';
import { rem } from '../../utils/functions/';

const ListNav = styled.ul`
    list-style: none;
    display: flex;
    flex-wrap: wrap;
    margin: 0;
    padding: 0;
`;

const ItemNav = styled.li`
    display: block;
    margin: 0;
    padding: 0;
`;

const LinkNav = styled.a`
    display: block;
    padding: ${rem('10px')} ${rem('25px')};
    font-size: ${rem('24px')};
    text-decoration: none;
    color: inherit;
`;

export { ListNav, ItemNav, LinkNav };
