import React from 'react';
import { ItemNav, LinkNav, ListNav } from './styled';


const Navigation = ({ className, options }) => (
    <nav className={className}>
        <ListNav>
            {options.map((opt, idx) => {
                return (<ItemNav key={`navid-` + idx}> <LinkNav href={opt.link}>{opt.name}</LinkNav></ItemNav>);
            })}
        </ListNav>
    </nav >
);

export default Navigation;