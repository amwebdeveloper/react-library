import React, {Component} from 'react';
import { Wrapper, Points, PointsBar } from './styled'

class Rating extends Component {
    constructor(props) {
        super(props);
        this.state = {points: 0.0};
    }

    componentDidMount() {
        const {points} = this.props;
        
        this.interval = setInterval(
            () =>  this.state.points <= points && this.updatePoints()
        , 0);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    updatePoints() {
        this.setState({points: this.state.points + 0.1});
    }

    render() {
        return (
            <Wrapper>
                <div>
                    <Points>{parseFloat(this.state.points).toFixed(1)}</Points> out of 10
                </div>
                <PointsBar points={parseFloat(this.state.points).toFixed(1)}></PointsBar>
            </Wrapper>
        );
    }
}
export default Rating;
