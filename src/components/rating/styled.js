import styled from 'styled-components';
import { rem } from '../../utils/functions/';

const Wrapper = styled.div`
    font-size: ${rem('24px')};
    color: ${props => props.theme.colors.disableFont};
`;

const Points = styled.strong`
    font-size: ${rem('56px')};
    color: ${props => props.theme.colors.font};
    font-weight: bold;
`;

const PointsBar = styled.div`
    position: relative;
    height: ${rem('8px')};
    border-radius: ${rem('4px')};
    background-color: ${props => props.theme.colors.lightgrey};
    overflow: hidden;
    

    &:after {
        will-change: width;
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        border-radius: ${rem('4px')};
        height: 100%;
        width: ${props => props.points && props.points * 10}%;
        background-color: ${props => props.theme.colors.green};
    }
`;

// default props
Wrapper.defaultProps = {
    theme: {
        colors: {
            disableFont: "grey"
        }
    }
};

Points.defaultProps = {
    theme: {
        colors: {
            font: "black"
        }
    }
};

PointsBar.defaultProps = {
    theme: {
        colors: {
            green: "lightgreen",
            red: "red",
            lightgrey: "lightgrey"
        }
    }
};

export { Wrapper, Points, PointsBar };
