import styled from 'styled-components';
import { rem } from '../../utils/functions/';

const StyleFooter = styled.footer`
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: ${rem('40px')} ${rem('80px')};
    background-color: ${props => props.theme.colors.font};
    color: ${props => props.theme.colors.white};
`;

StyleFooter.defaultProps = {
    theme: {
        colors: {
            white: 'black',
            font: 'white'
        }
    }
}

export { StyleFooter };
