import React from 'react';
import { StyleFooter } from './styled';

const Footer = ({ children }) => (
    <StyleFooter>
        {children}
    </StyleFooter>
);

export default Footer;