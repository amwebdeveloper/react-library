import styled from 'styled-components';
import { rem } from '../../utils/functions/';

const Wrapper = styled.div`
    width: 100%;
    @media only screen and (min-width: ${rem('768px')}) {
        display: flex;
        align-items: center;
    }
`;

const UserAvatar = styled.img`
    height: ${rem('120px')};
    min-width: ${rem('120px')};
    margin-bottom: ${rem('20px')};

    @media only screen and (min-width: ${rem('768px')}) {
        margin-right: ${rem('32px')};
    }
`;

const UserName = styled.h3`
    margin: 0;
    font-size: ${rem('24px')};
`;

const UserShortDescription = styled.p`
    margin: 0;
    color: grey;
    font-size: ${rem('20px')};
`;

export { Wrapper, UserAvatar, UserName, UserShortDescription };
