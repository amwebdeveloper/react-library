import React from 'react';
import { Wrapper, UserAvatar, UserName, UserShortDescription } from './styled'
import Example from '../../../assets/images/avatars/example.jpg';

const Profile = ({ className, userName, description }) => (
    <Wrapper className={className}>
        <UserAvatar src={Example} alt="" width="120" height="120" />
        <div>
            <UserName>{userName}</UserName>
            <UserShortDescription>{description}</UserShortDescription>
        </div>
    </Wrapper>
);

export default Profile;
