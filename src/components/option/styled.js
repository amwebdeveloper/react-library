import styled from 'styled-components';
import { rem } from '../../utils/functions/';

const IconButton = styled.img`
    margin-bottom: ${rem('5px')};
`;

const StyleName = styled.span`
    position: relative;
    color: ${props => props.theme.colors.fontSpecial};
`;

const Wrapper = styled.button`
    transition: box-shadow .15s ease;

    border: 0;
    border-radius: ${rem('8px')};

    position: relative;
    height: ${rem('184px')};
    width: ${rem('202px')};
    box-sizing: border-box;
    padding: ${rem('20px')};
    overflow: hidden;
    background: ${props => props.theme.colors.darkWhite};
    
    cursor: pointer;

    font-size: ${rem('24px')};
    font-family: inherit;
    font-weight: bold;
    color: ${props => props.theme.colors.fontSpecial};

    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    &:before {
        transition: all .25s ease;
        content: "";
        position: absolute;
        transition: all 0.2s ease;
        background: transparent;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
    }

    // state
    &:hover {
        background: transparent;
        box-shadow: 0 4px 16px 0 rgba(0,0,0,0.24);
        color: ${props => props.theme.colors.white};

        &:before {
            background: ${props => props.theme.colors.bgradient};
        }

        ${StyleName} { 
            color: ${props => props.theme.colors.white};
        }
        ${IconButton} {
            filter: brightness(50);
        }
    }
`;

StyleName.defaultProps = {
    theme: {
        colors: {
            fontSpecial: "grey"
        }
    }
}

Wrapper.defaultProps = {
    theme: {
        colors: {
            bgradient: "linear-gradient(90deg, #BBB 0%, #999 100%)",
            white: "white",
            darkWhite: "lightgrey",
            fontSpecial: "grey"
        }
    }
}

export { Wrapper, IconButton, StyleName };
