import React from 'react';
import { Wrapper, IconButton, StyleName } from './styled'
import { Calendar, Messages, Settings, Stats, Diffusion, Profile } from '../../../assets';

const Option = ({ description }) => {
    let icon = null;
    if (description === 'Calendar') {
        icon = Calendar;
    }
    if (description === 'Messages') {
        icon = Messages;
    }

    if (description === 'Settings') {
        icon = Settings;
    }

    if (description === 'Stats') {
        icon = Stats;
    }

    if (description === 'Diffusions') {
        icon = Diffusion;
    }

    if (description === 'Profile') {
        icon = Profile;
    }

    return (
        <Wrapper>
            <IconButton src={icon} alt="" width="56" height="56" />
            <StyleName>{description}</StyleName>
        </Wrapper>
    );
};

export default Option;
