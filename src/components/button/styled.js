import styled from 'styled-components';
import { rem } from '../../utils/functions/';

const StyleButton = styled.button`
    text-align: center;
    background-color: white;
    color: black;
    padding: ${rem('8px')} ${rem('32px')};
    margin: auto;
    text-transform: uppercase;
    font-weight: bold;
    border: ${rem('2px')} solid black;
    border-radius: ${rem('5px')};
    transition: all .15s ease;


    &:hover {
        background: black;
        color: white;
    }
`;

export { StyleButton };
