import React from 'react';
import { Brand, Navigation } from '../';
import { StyleHeader } from './styled';

const Header = ({ text, options }) => (
    <StyleHeader>
        <Brand text={text} />
        <Navigation options={options} />
    </StyleHeader>
);

export default Header;