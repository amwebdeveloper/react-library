import styled from 'styled-components';
import { rem } from '../../utils/functions/';

const StyleHeader = styled.header`
    background: ${props => props.theme.colors.bgradient};
    color: ${props => props.theme.colors.white};
    padding: ${rem('20px')};

    @media only screen and (min-width: ${rem('768px')}) {
        padding: ${rem('40px')} ${rem('80px')} ${rem('80px')};
        display: flex;
        justify-content: space-between;
        align-items: center;
    }
`;

StyleHeader.defaultProps = {
    theme: {
        colors: {
            bgradient: "white",
            white: "black"
        }
    }
}

export { StyleHeader };
