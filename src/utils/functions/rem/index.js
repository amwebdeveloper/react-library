const rem = (unit) => {
  if (typeof unit !== 'number') {
    unit = parseInt(unit, 10);
  }
  return `${unit * 0.0625}rem`;
}

export default rem;