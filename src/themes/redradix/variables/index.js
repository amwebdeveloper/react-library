const colors = {
    // color text
    font: "#484848",
    fontSpecial: "#C4CCE8",
    disableFont: "#AFAFAF",

    // grey scale
    white: "#FFFFFF",
    darkWhite: "#F0F2F9",
    lightgrey: "#D8D8D8",

    // red
    red: "#F56060",

    // green
    green: "#60F560",

    // gradient
    bgradient: "linear-gradient(90deg, #B993D6 0%, #8CA6DB 100%)"
};

const fonts = {
    mainFont: 'Open Sans'
};

export { colors, fonts }