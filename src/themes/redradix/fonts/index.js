import { css } from 'styled-components';
import Regular from '../../../../assets/fonts/OpenSans-Regular.ttf';
import Bold from '../../../../assets/fonts/OpenSans-Bold.ttf';
import Bolder from '../../../../assets/fonts/OpenSans-ExtraBold.ttf';
import Italic from '../../../../assets/fonts/OpenSans-Italic.ttf';

const fontFace = css`
    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: local('Open Sans Regular'),
             local('OpenSans-Regular'),
             url(${Regular}) format('truetype');
    }
    @font-face {
        font-family: 'Open Sans';
        font-style: italic;
        font-weight: 400;
        font-display: swap;
        src: local('Open Sans Italic'),
             local('OpenSans-Italic'),
             url(${Italic}) format('truetype');
    }
    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 600;
        font-display: swap;
        src: local('Open Sans SemiBold'),
             local('OpenSans-SemiBold'),
             url(${Bold}) format('truetype');
    }
    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 700;
        font-display: swap;
        src: local('Open Sans Bold'),
             local('OpenSans-Bold'),
             url(${Bolder}) format('truetype');
    }
`;

export { fontFace };
