import { createGlobalStyle } from 'styled-components';
// fonts
import { fontFace } from './fonts';
// variables
import { colors, fonts } from './variables';

const Global = createGlobalStyle`
    ${fontFace}

    body {
        margin: 0;
        padding: 0;
        font-family: "${fonts.mainFont}";
        font-size: 100%;
        color: ${colors.font};
    }
`;

const RedradixTheme = {
    colors,
    fonts
};

export { Global, RedradixTheme };