import React from 'react';
import { Header, Footer } from '../../components';
import { StyleMain, StyleTabsProfileUser } from './styled';

const Home = ({ text, active, options, userName, description, kpiOption, points, children, buttonOptions }) => (
    <div>
        <Header text={text} options={options} />
        <StyleMain>
            <StyleTabsProfileUser
                userName={userName}
                description={description}
                kpiOption={kpiOption}
                points={points}
                buttonOptions={buttonOptions}
                children={children}
                active={active}
            />
        </StyleMain>
        <Footer>
            Fake project &copy; 2019
        </Footer>
    </div>
);

export default Home;
