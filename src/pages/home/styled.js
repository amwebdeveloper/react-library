import styled from 'styled-components';
import { rem } from '../../utils/functions';
import { TabsProfileUser } from '../../views'

const StyleMain = styled.main`
    display: block;
    padding: ${rem('20px')};
    @media only screen and (min-width: ${rem('768px')}) {
        padding: ${rem('40px')} ${rem('80px')} ${rem('80px')};
    }

`;

const StyleTabsProfileUser = styled(TabsProfileUser)`
    max-width: ${rem('1280px')};
    border-radius: ${rem('8px')};
    box-shadow: 0 0 .2rem lightgray;
    margin: 0 auto 0 auto;
    background-color: ${props => props.theme.colors.white};

    @media only screen and (min-width: ${rem('1024px')}) {
        margin: ${rem('-85px')} auto 0 auto;
    }
`;

StyleTabsProfileUser.defaultProps = {
    theme: {
        colors: {
            white: "white"
        }
    }
};

export { StyleMain, StyleTabsProfileUser };
