import React from 'react';
import { action } from '@storybook/addon-actions';
import { Button, Navigation, Brand, Header, TabContent, TabBehaviour, Profile, Kpi, Rating, Description, Option, Footer } from '../components';
import { options } from '../../mocks';

export default {
  title: 'Core'
};

/**
 * Button
 */

export const Buttons = () => <Button onClick={action('clicked')} />;

/**
 * Navigation
 */
export const Navigations = () => <Navigation options={options} />;

/**
 * Brand
 */
export const Logo = () => <Brand text="CompanyLogo" />;

/**
 * Header
 */

export const MainHeader = () => <Header text="CompanyLogo" options={options} />;

/**
 * Tabs
 */
export const TabUser = () => 
    <TabBehaviour>
      <TabContent label="First tab">
        <p>Contenido del primer tab</p>
      </TabContent>
      <TabContent label="Second tab">
        <p>Contenido del segundo tab</p>
      </TabContent>
      <TabContent  label="Third tab">
        <p>Contenido del tercer tab</p>
      </TabContent>
      <TabContent  label="Fourth tab">
        <p>Contenido del cuarto tab</p>
      </TabContent>
    </TabBehaviour>;

/**
 * Profile
 */
export const ProfileUser = () => <Profile userName="User Name" description="User description lorem ipsum dolor sit amet" />

/**
 * Kpi
 */
export const Kpis = () => (
  <div>
    <h1>Up point</h1>
    <Kpi percentage={70} dataName="Fake data 001" />
    <br></br>
    <h1>Down point</h1>
    <Kpi percentage={33} dataName="Fake data 002" />
  </div>
);

/**
 * Rating
 */

export const RatingUser = () => <Rating points={8.2} />

/**
 * Description
 */
export const DescriptionUser = () =>
  <Description>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ac dapibus lacus, non dapibus nisl.
     Proin faucibus mollis urna, a ornare lorem finibus ac. Proin finibus sapien erat, pellentesque vulputate
     orci facilisis sed. Duis in auctor magna. Cras sagittis porta mauris nec porta. Ut a nisl maximus, pulvinar
     dolor ut, tempor tellus. Duis dapibus convallis quam, non tincidunt nunc aliquet nec. Fusce semper est et
     tellus semper sodales et ut dui. Etiam rutrum sapien diam, convallis varius arcu rutrum sed. Maecenas id
     mauris vitae ligula varius semper.</p>
    <p>Maecenas a sapien feugiat, viverra justo sed, pharetra elit. Duis nec lorem feugiat nunc mollis euismod.
     Phasellus dignissim lacus in nisl bibendum, id suscipit eros euismod. Cras sagittis tortor et auctor aliquet.
     In ligula elit, sodales non elit id, dapibus maximus turpis. Phasellus congue sapien non elementum lobortis.
     Maecenas viverra nibh purus, vel fringilla ante faucibus ac. Nulla vestibulum elit metus, et ullamcorper
     libero sodales sed. Etiam efficitur purus fringilla, pulvinar sapien id, placerat sapien. Fusce facilisis
     ipsum ac turpis ullamcorper, sit amet hendrerit arcu lobortis. Pellentesque scelerisque ipsum libero.
     Mauris sapien arcu, consectetur at nisi ac, facilisis tristique felis.</p>
    <p>Maecenas a sapien feugiat, viverra justo sed, pharetra elit. Duis nec lorem feugiat nunc mollis euismod.
     Phasellus dignissim lacus in nisl bibendum, id suscipit eros euismod. Cras sagittis tortor et auctor aliquet.
     In ligula elit, sodales non elit id, dapibus maximus turpis. Phasellus congue sapien non elementum lobortis.
     Maecenas viverra nibh purus, vel fringilla ante faucibus ac. Nulla vestibulum elit metus, et ullamcorper
     libero sodales sed. Etiam efficitur purus fringilla, pulvinar sapien id, placerat sapien. Fusce facilisis
     ipsum ac turpis ullamcorper, sit amet hendrerit arcu lobortis. Pellentesque scelerisque ipsum libero.
     Mauris sapien arcu, consectetur at nisi ac, facilisis tristique felis.</p>
    <p>Maecenas a sapien feugiat, viverra justo sed, pharetra elit. Duis nec lorem feugiat nunc mollis euismod.
     Phasellus dignissim lacus in nisl bibendum, id suscipit eros euismod. Cras sagittis tortor et auctor aliquet.
     In ligula elit, sodales non elit id, dapibus maximus turpis. Phasellus congue sapien non elementum lobortis.
     Maecenas viverra nibh purus, vel fringilla ante faucibus ac. Nulla vestibulum elit metus, et ullamcorper
     libero sodales sed. Etiam efficitur purus fringilla, pulvinar sapien id, placerat sapien. Fusce facilisis
     ipsum ac turpis ullamcorper, sit amet hendrerit arcu lobortis. Pellentesque scelerisque ipsum libero.
     Mauris sapien arcu, consectetur at nisi ac, facilisis tristique felis.</p>
  </Description>

/**
 * Option
 */

export const OptionsUser = () => <Option description="Calendar" />

/**
 * Footer
 */

export const FooterPage = () =>
  <Footer>
    Fake Project &copy; 2019
  </Footer>