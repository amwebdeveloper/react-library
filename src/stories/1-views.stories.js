import React from 'react';
import { kpiOptions, buttonOptions, tabs } from '../../mocks';
import { ProfileUser, TabsProfileUser } from '../views';

export default {
    title: 'Views'
};

export const MainProfileUser = () =>
    <ProfileUser
        userName="User Name"
        description="User description lorem ipsum dolor sit amet"
        kpiOption={kpiOptions}
        points={7.4}
        buttonOptions={buttonOptions}
    >
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ac dapibus lacus, non dapibus nisl.
        Proin faucibus mollis urna, a ornare lorem finibus ac. Proin finibus sapien erat, pellentesque vulputate
        orci facilisis sed. Duis in auctor magna. Cras sagittis porta mauris nec porta. Ut a nisl maximus, pulvinar
        dolor ut, tempor tellus. Duis dapibus convallis quam, non tincidunt nunc aliquet nec. Fusce semper est et
        tellus semper sodales et ut dui. Etiam rutrum sapien diam, convallis varius arcu rutrum sed. Maecenas id
        mauris vitae ligula varius semper.</p>
        <p>Maecenas a sapien feugiat, viverra justo sed, pharetra elit. Duis nec lorem feugiat nunc mollis euismod.
        Phasellus dignissim lacus in nisl bibendum, id suscipit eros euismod. Cras sagittis tortor et auctor aliquet.
        In ligula elit, sodales non elit id, dapibus maximus turpis. Phasellus congue sapien non elementum lobortis.
        Maecenas viverra nibh purus, vel fringilla ante faucibus ac. Nulla vestibulum elit metus, et ullamcorper
        libero sodales sed. Etiam efficitur purus fringilla, pulvinar sapien id, placerat sapien. Fusce facilisis
        ipsum ac turpis ullamcorper, sit amet hendrerit arcu lobortis. Pellentesque scelerisque ipsum libero.
        Mauris sapien arcu, consectetur at nisi ac, facilisis tristique felis.</p>
        <p>Maecenas a sapien feugiat, viverra justo sed, pharetra elit. Duis nec lorem feugiat nunc mollis euismod.
        Phasellus dignissim lacus in nisl bibendum, id suscipit eros euismod. Cras sagittis tortor et auctor aliquet.
        In ligula elit, sodales non elit id, dapibus maximus turpis. Phasellus congue sapien non elementum lobortis.
        Maecenas viverra nibh purus, vel fringilla ante faucibus ac. Nulla vestibulum elit metus, et ullamcorper
        libero sodales sed. Etiam efficitur purus fringilla, pulvinar sapien id, placerat sapien. Fusce facilisis
        ipsum ac turpis ullamcorper, sit amet hendrerit arcu lobortis. Pellentesque scelerisque ipsum libero.
        Mauris sapien arcu, consectetur at nisi ac, facilisis tristique felis.</p>
        <p>Maecenas a sapien feugiat, viverra justo sed, pharetra elit. Duis nec lorem feugiat nunc mollis euismod.
        Phasellus dignissim lacus in nisl bibendum, id suscipit eros euismod. Cras sagittis tortor et auctor aliquet.
        In ligula elit, sodales non elit id, dapibus maximus turpis. Phasellus congue sapien non elementum lobortis.
        Maecenas viverra nibh purus, vel fringilla ante faucibus ac. Nulla vestibulum elit metus, et ullamcorper
        libero sodales sed. Etiam efficitur purus fringilla, pulvinar sapien id, placerat sapien. Fusce facilisis
        ipsum ac turpis ullamcorper, sit amet hendrerit arcu lobortis. Pellentesque scelerisque ipsum libero.
        Mauris sapien arcu, consectetur at nisi ac, facilisis tristique felis.</p>
    </ProfileUser>;

export const TabsUser = () =>
    <TabsProfileUser
        userName="User Name"
        description="User description lorem ipsum dolor sit amet"
        kpiOption={kpiOptions}
        points={7.4}
        buttonOptions={buttonOptions}
        tabs={tabs}
        label="First tab"
    >
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ac dapibus lacus, non dapibus nisl.
        Proin faucibus mollis urna, a ornare lorem finibus ac. Proin finibus sapien erat, pellentesque vulputate
        orci facilisis sed. Duis in auctor magna. Cras sagittis porta mauris nec porta. Ut a nisl maximus, pulvinar
        dolor ut, tempor tellus. Duis dapibus convallis quam, non tincidunt nunc aliquet nec. Fusce semper est et
        tellus semper sodales et ut dui. Etiam rutrum sapien diam, convallis varius arcu rutrum sed. Maecenas id
        mauris vitae ligula varius semper.</p>
        <p>Maecenas a sapien feugiat, viverra justo sed, pharetra elit. Duis nec lorem feugiat nunc mollis euismod.
        Phasellus dignissim lacus in nisl bibendum, id suscipit eros euismod. Cras sagittis tortor et auctor aliquet.
        In ligula elit, sodales non elit id, dapibus maximus turpis. Phasellus congue sapien non elementum lobortis.
        Maecenas viverra nibh purus, vel fringilla ante faucibus ac. Nulla vestibulum elit metus, et ullamcorper
        libero sodales sed. Etiam efficitur purus fringilla, pulvinar sapien id, placerat sapien. Fusce facilisis
        ipsum ac turpis ullamcorper, sit amet hendrerit arcu lobortis. Pellentesque scelerisque ipsum libero.
        Mauris sapien arcu, consectetur at nisi ac, facilisis tristique felis.</p>
        <p>Maecenas a sapien feugiat, viverra justo sed, pharetra elit. Duis nec lorem feugiat nunc mollis euismod.
        Phasellus dignissim lacus in nisl bibendum, id suscipit eros euismod. Cras sagittis tortor et auctor aliquet.
        In ligula elit, sodales non elit id, dapibus maximus turpis. Phasellus congue sapien non elementum lobortis.
        Maecenas viverra nibh purus, vel fringilla ante faucibus ac. Nulla vestibulum elit metus, et ullamcorper
        libero sodales sed. Etiam efficitur purus fringilla, pulvinar sapien id, placerat sapien. Fusce facilisis
        ipsum ac turpis ullamcorper, sit amet hendrerit arcu lobortis. Pellentesque scelerisque ipsum libero.
        Mauris sapien arcu, consectetur at nisi ac, facilisis tristique felis.</p>
        <p>Maecenas a sapien feugiat, viverra justo sed, pharetra elit. Duis nec lorem feugiat nunc mollis euismod.
        Phasellus dignissim lacus in nisl bibendum, id suscipit eros euismod. Cras sagittis tortor et auctor aliquet.
        In ligula elit, sodales non elit id, dapibus maximus turpis. Phasellus congue sapien non elementum lobortis.
        Maecenas viverra nibh purus, vel fringilla ante faucibus ac. Nulla vestibulum elit metus, et ullamcorper
        libero sodales sed. Etiam efficitur purus fringilla, pulvinar sapien id, placerat sapien. Fusce facilisis
        ipsum ac turpis ullamcorper, sit amet hendrerit arcu lobortis. Pellentesque scelerisque ipsum libero.
        Mauris sapien arcu, consectetur at nisi ac, facilisis tristique felis.</p>
    </TabsProfileUser>