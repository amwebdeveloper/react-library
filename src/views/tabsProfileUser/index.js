import React from 'react';
import { TabContent, TabBehaviour } from '../../components';
import { ProfileUser } from '../';

const TabsProfileUser = ({ className, userName, description, kpiOption, points, children, buttonOptions }) => (
    <div className={className}>
        <TabBehaviour>
            <TabContent label="First tab">
                <ProfileUser
                    userName={userName}
                    description={description}
                    kpiOption={kpiOption}
                    points={points}
                    buttonOptions={buttonOptions}
                >
                    {children}
                </ProfileUser>
            </TabContent>
            <TabContent  label="Second tab">
                Content tab 2
            </TabContent>
            <TabContent  label="Third tab">
                Content tab 3
            </TabContent>
            <TabContent  label="Fourth tab">
                Content tab 4
            </TabContent>
        </TabBehaviour>
    </div>
);

export default TabsProfileUser;
