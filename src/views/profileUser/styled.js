import styled from 'styled-components';
import { rem } from '../../utils/functions';

const StyleBlock = styled.div`
    display: flex;
    justify-content: space-between;
    flex-flow: wrap;
    padding: ${rem('30px')} 0;
    margin-bottom: ${rem('30px')};
    width: 100%;
    border-bottom: ${rem('1px')} solid ${props => props.theme.colors.lightgrey};

    &:last-child {
        border: 0;
    }
`;

const StyleBlockCenter = styled(StyleBlock)`
    align-items: center;
`;

const StyledBlockWrap = styled(StyleBlock)`
    box-sizing: border-box;

    @media only screen and (min-width: ${rem('1024px')}) {
        flex-flow: wrap;
        flex: 1 1 auto;
        max-width: calc(100% - 515px);
        align-content: flex-start;
        justify-content: space-around;
    }
`;

const StyleChildBlock = styled.div`
    position: relative;
    box-sizing: border-box;

    @media only screen and (min-width: ${rem('1024px')}) {
        max-width: ${rem('430px')};

        &:after {
            position: absolute;
            top: 0;
            bottom: 0;
            margin: auto;
            right: ${rem('-45px')};
            content: "";
            height: 100%;
            width: ${rem('1px')};
            background: ${props => props.theme.colors.lightgrey};
        }
    }
`;

const StyledItem = styled.div`
    flex-basis: 100%;
    display: flex;
    justify-content: center;
    margin-bottom: ${rem('13px')};
    box-sizing: border-box;

    @media only screen and (min-width: ${rem('768px')}) {
        flex-basis: calc((100% / 3) - 26px);
    }
`;

StyleBlock.defaultProps = {
    theme: {
        colors: {
            lightgrey: 'lightgrey'
        }
    }
};

StyleBlockCenter.defaultProps = {
    theme: {
        colors: {
            lightgrey: 'lightgrey'
        }
    }
};

StyledBlockWrap.defaultProps = {
    theme: {
        colors: {
            lightgrey: 'lightgrey'
        }
    }
};

StyleChildBlock.defaultProps = {
    theme: {
        colors: {
            lightgrey: 'lightgrey'
        }
    }
};

export { StyleBlock, StyleBlockCenter, StyledBlockWrap, StyleChildBlock, StyledItem };
