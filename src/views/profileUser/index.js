import React from 'react';
import { Profile, Description, Rating, Kpi, Option } from '../../components';
import { StyleBlock, StyledBlockWrap, StyleChildBlock, StyledItem, StyleBlockCenter } from './styled';

const ProfileUser = ({ userName, description, kpiOption, points, children, buttonOptions }) => (
    <div>
        <StyleBlockCenter>
            <StyleChildBlock>
                <Profile userName={userName} description={description} />
            </StyleChildBlock>

            <StyledBlockWrap>
                {kpiOption.map((opt, idx) =>
                    <Kpi key={`kpi-` + idx} good={opt.good} percentage={opt.percentage} dataName={opt.dataName} />
                )}
            </StyledBlockWrap>
        </StyleBlockCenter>

        <StyleBlock>
            <StyleChildBlock>
                <Rating points={points} />
                <Description>
                    {children}
                </Description>
            </StyleChildBlock>

            <StyledBlockWrap>
                {buttonOptions.map((opt, idx) =>
                    <StyledItem key={`bitem-` + idx}>
                        <Option description={opt.description} />
                    </StyledItem>
                )}
            </StyledBlockWrap>
        </StyleBlock>
    </div>
);

export default ProfileUser;
