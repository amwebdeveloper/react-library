// you can use this file to add your custom webpack plugins, loaders and anything you like.
// This is just the basic way to add addional webpack configurations.
// For more information refer the docs: https://getstorybook.io/docs/configurations/custom-webpack-config

// IMPORTANT
// When you add this file, we won't add the default configurations which is similar
// to "React Create App". This only has babel loader to load JavaScript.
const path = require('path');

module.exports = {
    mode: 'development',
    watch: true,
    watchOptions: {
        ignored: ['files/**/*.js', 'node_modules'],
        aggregateTimeout: 300,
        poll: 1000
    },
    module: {
        rules: [
            {
                test: /\.(woff|ttf|svg|jpg|gif|png)$/,
                loader: 'file-loader'
            }
        ]
    }
};
